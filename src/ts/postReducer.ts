const initialState = 0;
export type Action = {
    type: string,
    payload?: number
}

export const postsReducer = (state: number = initialState, action: Action) => {

    switch (action.type) {
        case 'piu':
            return state + action.payload;
        case 'meno':
            return state - action.payload;
        default:
            return state;
    }


}





export default postsReducer;