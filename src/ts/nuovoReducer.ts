export type ActionNuovo = {
    type: string
    numero: number
}


export const nuovoReducer = (state: number = 0, action: ActionNuovo) => {
    switch (action.type) {
        case 'nuovo':
            return state + action.numero;
        default:
            return state
    }
}