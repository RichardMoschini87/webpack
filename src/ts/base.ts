export default class Base {
    htmlType: string;
    elemento: HTMLElement;
    parentElement: HTMLElement;
    id: string;
    className: string;

    constructor(type: string, appendParent: HTMLElement, className: string, id: string) {
        this.htmlType = type
        this.parentElement = appendParent;
        this.className = className;
        this.id = id;
    }

    createElem(): void {
        this.elemento = document.createElement(this.htmlType);
        this.elemento.className = this.className;
        this.elemento.id = this.id;
        this.parentElement != null ? this.parentElement.appendChild(this.elemento) : document.body.appendChild(this.elemento);
    }

    getElem(): HTMLElement {
        return this.elemento;
    }
} 