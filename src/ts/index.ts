import Nav from "./nav";
import '../css/stile.scss'
import '../css/main.scss'
import NavValue from '../js/nav'
import { store } from "./reduxStore";
import { Action } from "./postReducer";
import { ActionNuovo } from "./nuovoReducer";

// variabile store redux
const stored = store;

//################# AZIONI REDUX ####
const action: Action = {
    type: 'piu',
    payload: 3
};

const nuovo: ActionNuovo = {
    type: 'nuovo',
    numero: 4
}
//##################################
console.log(stored.getState());

// Creazione di una navbar
const navBar = new Nav('nav', null, null, null);
navBar.createElem();

//Crea la lista degli elementi della Nav
const navValue = new NavValue();
navBar.creaListaElementi(navValue.primo, navValue.secondo, navValue.terzo);

// metto la nav nel mio div
const divNav = document.getElementById('myDiv');
divNav.appendChild(navBar.getElem());




let h2 = document.createElement('h1');
h2.innerHTML = 'PROVA';

// Custom event
h2.addEventListener('changecolor', (e: CustomEvent) => {
    h2.style.color = e.detail.color;
})
divNav.appendChild(h2);

// bottone per prova redux
const button = document.createElement('button');
button.innerHTML = 'BOTTONE';
divNav.appendChild(button);

button.onclick = () => {
    stored.dispatch(action);
    stored.dispatch(nuovo);
    stored.dispatch({ type: 'CIAO' });
    changeColor();
}

store.subscribe(() => {
    console.log(stored.getState().posts)
    if (stored.getState().posts > 0) {
        let h1 = document.createElement('h1');
        h1.innerHTML = 'stored' + ' ' + stored.getState().posts;
        divNav.appendChild(h1);
    }
})

function changeColor() {
    const customEv = new CustomEvent('changecolor', {
        detail: {
            color: 'red'
        }
    })
    h2.dispatchEvent(customEv);
}





