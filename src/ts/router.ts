export default class Router {
    paths: { key: string, template: string }[] = [
        { key: 'home', template: '<h1>HOME</h1>' },
        { key: 'cerca', template: '<h1>CERCA</h1>' },
        { key: 'record', template: '<h1>RECORD</h1>' }
    ]

    constructor() {
    }


    load(page: string = 'home') {
        console.log(page);

        const route = this.paths.find(f => f.key === page);

        console.log('prova di -> ' + route.template);

        window.history.pushState({}, 'done', route.key);

        window.addEventListener('popstate', (event) => {
            console.log("location: " + document.location.pathname + ", state: " + JSON.stringify(event.state));
            this.load(document.location.pathname.substring(1,document.location.pathname.length))
        });

        var prova = document.getElementById('prova');
        if (prova == null) {
            prova = document.createElement('h1');
            prova.id = 'prova';
        }
        console.log(prova)
        prova.innerHTML = route.template;
        document.getElementById('myDiv').appendChild(prova);

        //.innerHTML = route.template;

    }

}

