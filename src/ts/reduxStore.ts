import { configureStore, Store } from '@reduxjs/toolkit'
import { nuovoReducer } from './nuovoReducer'
import { Action, postsReducer } from './postReducer'
import { reducerTest } from './reducerTest'
// ...

// Midleware sulla action 
function noApply(store: Store) {
  return function (next: any) {
    return function (action: Action) {
      console.log(store.getState() + 'ciao');
      return next(action);
    }
  }
}
export const store = configureStore({
  reducer: {
    posts: postsReducer,
    nuovo: nuovoReducer,
    test: reducerTest
  },
  middleware: [
    noApply
  ]
})


