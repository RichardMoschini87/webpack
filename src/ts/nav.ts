import Base from "./base";
import '../css/nav.scss';
import Router from "./router";

export default class Nav extends Base {
    listaElement: Map<string, HTMLElement> = new Map<string, HTMLElement>();

    constructor(type: string, appendParent: HTMLElement, className: string, id: string) {
        super(type, appendParent, className, id);
    }

    creaListaElementi(...elems: string[]): void {
        if (elems.length > 3)
            throw new Error('non si possono inserire più di tre elementi nella nav')
        elems.forEach(el => {
            let html = document.createElement('a');
            html.className = 'a-nav';
            html.id = el.toLocaleLowerCase();
            html.innerHTML = el;
            this.listaElement.set(el, html);
            const router = new Router();
            
            html.addEventListener('click', (e) => {
                router.load(el.toLocaleLowerCase());
            })

            super.getElem().appendChild(html);
        })
    }

    getListaElementi(): Map<string, HTMLElement> {
        return this.listaElement;
    }
}