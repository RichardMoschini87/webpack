
export default class NavValue {
    #first
    #second
    #third
    constructor() {
        this.#first = 'Home'
        this.#second = 'Cerca'
        this.#third = 'Record'
    }

    get primo() {
        return this.#first
    }
    get secondo() {
        return this.#second
    }
    get terzo() {
        return this.#third
    }
}